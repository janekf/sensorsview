﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using SensorLibrary;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Media3D;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SensorsView
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private AmbientLightSensor _ambientLightSensor;
        private CompassSensor _compassSensor;
        private AccelerometerSensor _accelerometerSensor;
        private GyrometerSensor _gyrometerSensor;
        private InclinometerSensor _inclinometerSensor;
        private DeviceOrientationSensor _orientationSensor;
        private GelocationSensor _gelocationSensor;
        private NfcSensor _nfcSensor;

        private CoreDispatcher _dispatcher; 

        public MainPage()
        {
            this.InitializeComponent();
            _dispatcher = Dispatcher;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            try
            {
                ActivateLightSensor();

                ActivateCompassSensor();

                ActivateAccelerationSensor();

                ActivateGyrometerSensor();

                ActivateInclinomterSensor();

                ActivateOrientationSensor();

                ActivateGeolocationSensor();

                ActivateNfcSensor();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Message: {0}\r\n Stack: {1}", ex.Message, ex.StackTrace);
                throw ex;
            }
        }

        private void ActivateNfcSensor()
        {
            _nfcSensor = new NfcSensor {Enabled = true};

            _nfcSensor.PeerFound +=
                async message => await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () =>
                        {
                            Nfc.Text = String.IsNullOrEmpty(message)
                                ? "No Message Recieved"
                                : message;
                        });

            _nfcSensor.PeerLost +=
                async () => await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () =>
                        {
                            Nfc.Text = "Divice has leave the Sensor Range";
                        });
        }

        private void ActivateGeolocationSensor()
        {
            _gelocationSensor = new GelocationSensor();

            _gelocationSensor.PositionChanged +=
                async position => await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () =>
                        {
                            Location.Text = String.Format("{0} - {1}\r\n{2}\r\n{3}",
                                position.CivicAddress.Country, 
                                position.CivicAddress.City, 
                                position.Coordinate.Longitude,
                                position.Coordinate.Latitude);
                        });
        }

        private void ActivateOrientationSensor()
        {
            _orientationSensor = new DeviceOrientationSensor();

            _orientationSensor.OrientationChanged +=
                async (orientation,rotationMatrix) => await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () =>
                        {
                            var projection = new PlaneProjection
                                                 {
                                                    RotationX = orientation.X * 110,
                                                    RotationY = orientation.Y * 110,
                                                    RotationZ = orientation.Z * 110
                                                 };

                            Orientation.Projection = projection;
                        });
        }

        private void ActivateInclinomterSensor()
        {
            _inclinometerSensor = new InclinometerSensor();

            _inclinometerSensor.TiltChanged +=
                async tilt => await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () =>
                    {
                        var rotateX = new RotateTransform
                        {
                            Angle = tilt.Roll
                        };
                        var rotateY = new RotateTransform
                        {
                            Angle = tilt.Pitch
                        };
                        var rotateZ = new RotateTransform
                        {
                            Angle = tilt.Yaw
                        };

                        Roll.RenderTransform = rotateX;
                        Pitch.RenderTransform = rotateY;
                        Yaw.RenderTransform = rotateZ;
                    });
        }

        private void ActivateGyrometerSensor()
        {
            _gyrometerSensor = new GyrometerSensor();

            _gyrometerSensor.AngularChanged +=
                async angular => await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () =>
                        {
                            var rotateX = new RotateTransform
                                               {
                                                   Angle = angular.X
                                               };
                            var rotateY = new RotateTransform
                                              {
                                                  Angle = angular.Y
                                              };
                            var rotateZ = new RotateTransform
                                              {
                                                  Angle = angular.Z
                                              };

                            AngelX.RenderTransform = rotateX;
                            AngelY.RenderTransform = rotateY;
                            AngelZ.RenderTransform = rotateZ;
                        });
        }

        private void ActivateAccelerationSensor()
        {
            _accelerometerSensor = new AccelerometerSensor();

            _accelerometerSensor.AccelerationChanged +=
                async acceleration => await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () =>
                        {
                            var offsetX = new PlaneProjection
                                              {
                                                  GlobalOffsetX = 55 * acceleration.X
                                              };
                            var offsetY = new PlaneProjection
                                              {
                                                  GlobalOffsetX = 55 * acceleration.Y
                                              };
                            var offsetZ = new PlaneProjection
                                              {
                                                  GlobalOffsetX = 55 * acceleration.Z + 55
                                              };
                            AccX.Projection = offsetX;
                            AccY.Projection = offsetY;
                            AccZ.Projection = offsetZ;
                        });
        }

        private void ActivateCompassSensor()
        {
            _compassSensor = new CompassSensor();

            _compassSensor.DirectionChanged +=
                async direction => await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () =>
                        {
                            var rotate = new CompositeTransform {Rotation = direction.MagneticNorth};

                            CompassPin.RenderTransform = rotate;
                            CompassCurrentDirectionMagneticNorth.Text = direction.MagneticNorth.ToString();
                        });
        }

        private void ActivateLightSensor()
        {
            _ambientLightSensor = new AmbientLightSensor();

            _ambientLightSensor.BrighnessChanged += 
                async lux => await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, 
                    () =>
                        {
                            LightSensorCurrentIlluminace.Text = lux.ToString();
                            var color = new Color
                                            {
                                                R = 255,
                                                G = 255,
                                                B = 255,
                                                A = (byte)(255 * lux/3500)
                                            };
                            LightSensorItem.Background = new SolidColorBrush(color);
                        });
        }

        private void TappedOnLightSensor(object sender, TappedRoutedEventArgs e)
        {
            _ambientLightSensor.Enabled = !_ambientLightSensor.Enabled;
        }

        private void TappedOnCompassSensor(object sender, TappedRoutedEventArgs e)
        {
            _compassSensor.Enabled = !_compassSensor.Enabled;
        }

        private void TappedOnAccelerationSensor(object sender, TappedRoutedEventArgs e)
        {
            _accelerometerSensor.Enabled = !_accelerometerSensor.Enabled;
        }

        private void TappedOnGyrometerSensor(object sender, TappedRoutedEventArgs e)
        {
            _gyrometerSensor.Enabled = !_gyrometerSensor.Enabled;
        }

        private void TappedOnInclinometerSensor(object sender, TappedRoutedEventArgs e)
        {
            _inclinometerSensor.Enabled = !_inclinometerSensor.Enabled;
        }

        private void TappedOnOrientationSensor(object sender, TappedRoutedEventArgs e)
        {
            _orientationSensor.Enabled = !_orientationSensor.Enabled;
        }

        private void TappedOnGeolocationSensor(object sender, TappedRoutedEventArgs e)
        {
            _gelocationSensor.Enabled = !_gelocationSensor.Enabled;
        }
    }
}