namespace SensorLibrary
{
    public class Tilt
    {
        public float Roll { get; set; }
        public float Pitch { get; set; }
        public float Yaw { get; set; }
    }
}