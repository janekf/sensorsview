using System;
using System.Diagnostics;
using Windows.Devices.Geolocation;

namespace SensorLibrary
{
    public class GelocationSensor : Sensor
    {
        public event Action<Geoposition> PositionChanged;
 
        private readonly Geolocator _gelocator;

        public GelocationSensor() : base("Geolocation Sensors")
        {
            _gelocator = new Geolocator {ReportInterval = 1000};
        }

        private void GelocatorOnStatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            Debug.WriteLine(args.Status);
        }

        private void GelocatorOnPositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            if(PositionChanged != null)
                PositionChanged(args.Position);
        }

        private bool _sensorIsEnabled;
        public override bool Enabled { 
            get { return _sensorIsEnabled; } 
            set
            {
                if(value == true && _sensorIsEnabled == false)
                {
                    _gelocator.PositionChanged += GelocatorOnPositionChanged;
                    _gelocator.StatusChanged += GelocatorOnStatusChanged;
                }
                else
                {
                    _gelocator.PositionChanged -= GelocatorOnPositionChanged;
                    _gelocator.StatusChanged -= GelocatorOnStatusChanged;
                }

                _sensorIsEnabled = value;
            } 
        }

    }
}