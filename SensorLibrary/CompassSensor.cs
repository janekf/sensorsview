using System;
using Windows.Devices.Sensors;

namespace SensorLibrary
{
    public class CompassSensor : Sensor
    {
        public event Action<Direction> DirectionChanged;
 
        private readonly Compass _compass;

        public CompassSensor()
            : base("Compass")
        {
            _compass = Compass.GetDefault();

            if(_compass == null) return;

            _compass.ReportInterval = _compass.MinimumReportInterval > 200
                                          ? _compass.MinimumReportInterval
                                          : 200;
        }

        private void CompassOnReadingChanged(Compass sender, CompassReadingChangedEventArgs args)
        {
            var direction = new Direction
                                {
                                    MagneticNorth = args.Reading.HeadingMagneticNorth,
                                    GeographicNorth = args.Reading.HeadingTrueNorth
                                };

            if (DirectionChanged != null)
                DirectionChanged(direction);
        }

        private bool _sensorIsEnabled;
        public override bool Enabled { 
            get { return _sensorIsEnabled; } 
            set
            {
                if(value == true && _sensorIsEnabled == false)
                    _compass.ReadingChanged += CompassOnReadingChanged;
                else
                    _compass.ReadingChanged -= CompassOnReadingChanged;

                _sensorIsEnabled = value;
            } 
        }

    }
}