using System;
using Windows.Devices.Sensors;

namespace SensorLibrary
{
    public class AmbientLightSensor : Sensor
    {
        public event Action<float> BrighnessChanged; 

        private readonly LightSensor _lightSensor;

        public AmbientLightSensor() : base("Light Sensor")
        {
            _lightSensor = LightSensor.GetDefault();

            if (_lightSensor == null) return;
            
            _lightSensor.ReportInterval = _lightSensor.MinimumReportInterval > 100
                                              ? _lightSensor.MinimumReportInterval
                                              : 100;
        }

        private void LightSensorOnReadingChanged(Windows.Devices.Sensors.LightSensor sender, LightSensorReadingChangedEventArgs args)
        {

            CurrentLightValue = args.Reading.IlluminanceInLux;

            if(BrighnessChanged != null)
                BrighnessChanged(CurrentLightValue);
        }

        public float CurrentLightValue { get; set; }

        private bool _sensorIsEnabled;
        public override bool Enabled { 
            get { return _sensorIsEnabled; } 
            set
            {
                if(value == true && _sensorIsEnabled == false)
                    _lightSensor.ReadingChanged += LightSensorOnReadingChanged;
                else
                    _lightSensor.ReadingChanged -= LightSensorOnReadingChanged;

                _sensorIsEnabled = value;
            } 
        }
    }
}