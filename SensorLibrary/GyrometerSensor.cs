using System;
using Windows.Devices.Sensors;

namespace SensorLibrary
{
    public class GyrometerSensor : Sensor
    {
        public event Action<Angular> AngularChanged;

        private readonly Gyrometer _gyrometer;

        public GyrometerSensor() : base("Gyrometer Sensor")
        {
            _gyrometer = Gyrometer.GetDefault();

            if (_gyrometer == null) return;

            _gyrometer.ReportInterval = _gyrometer.MinimumReportInterval > 110
                                            ? _gyrometer.MinimumReportInterval
                                            : 110;
        }

        private void GyrometerOnReadingChanged(Gyrometer sender, GyrometerReadingChangedEventArgs args)
        {
            var reading = args.Reading;

            if (AngularChanged != null)
                AngularChanged(new Angular
                                   {
                                       X = reading.AngularVelocityX,
                                       Y = reading.AngularVelocityY,
                                       Z = reading.AngularVelocityZ
                                   });

        }

        private bool _sensorIsEnabled;
        public override bool Enabled { 
            get { return _sensorIsEnabled; } 
            set
            {
                if(value == true && _sensorIsEnabled == false)
                    _gyrometer.ReadingChanged += GyrometerOnReadingChanged;
                else
                    _gyrometer.ReadingChanged -= GyrometerOnReadingChanged;

                _sensorIsEnabled = value;
            } 
        }

    }
}