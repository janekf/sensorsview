using System;
using Windows.Devices.Sensors;

namespace SensorLibrary
{
    public class DeviceOrientationSensor : Sensor
    {
        public event Action<Orentation,SensorRotationMatrix> OrientationChanged;

        private OrientationSensor _orientationSensor;

        public DeviceOrientationSensor()
            : base("Orientation Sensor")
        {
            _orientationSensor = OrientationSensor.GetDefault();

            if (_orientationSensor != null)
            {
                _orientationSensor.ReportInterval = _orientationSensor.MinimumReportInterval > 110
                                                        ? _orientationSensor.MinimumReportInterval
                                                        : 110;
            }

        }

        private void OrientationSensorOnReadingChanged(OrientationSensor sender, OrientationSensorReadingChangedEventArgs args)
        {
            var sensorReading = args.Reading;

            if(OrientationChanged != null)
                OrientationChanged(new Orentation
                                       {
                                           X = sensorReading.Quaternion.X,
                                           Y= sensorReading.Quaternion.Y,
                                           Z = sensorReading.Quaternion.Z
                                       }, 
                                   sensorReading.RotationMatrix);
        }

        private bool _sensorIsEnabled;
        public override bool Enabled { 
            get { return _sensorIsEnabled; } 
            set
            {
                if(value == true && _sensorIsEnabled == false)
                    _orientationSensor.ReadingChanged += OrientationSensorOnReadingChanged;
                else
                    _orientationSensor.ReadingChanged -= OrientationSensorOnReadingChanged;

                _sensorIsEnabled = value;
            } 
        }

    }
}