﻿using System;
using Windows.UI.Xaml.Controls;

namespace SensorLibrary
{
    public abstract class Sensor
    {
        protected Sensor(String name)
        {
            Name = name;
        }

        protected string Name { get; set; }

        public abstract bool Enabled { get; set; }
    }
}
