using System;
using Windows.Devices.Sensors;

namespace SensorLibrary
{
    public class AccelerometerSensor : Sensor
    {
        public event Action<Acceleration> AccelerationChanged;

        private readonly Accelerometer _accelerometer;

        public AccelerometerSensor() : base("Acelerometer Sensor")
        {
            _accelerometer = Accelerometer.GetDefault();

            if (_accelerometer == null) return;

            _accelerometer.ReportInterval = _accelerometer.MinimumReportInterval > 100
                                                ? _accelerometer.MinimumReportInterval
                                                : 100;
        }

        private void AccelerometerOnReadingChanged(Accelerometer sender, AccelerometerReadingChangedEventArgs args)
        {
            var reading = args.Reading;

            if(AccelerationChanged != null)
                AccelerationChanged(new Acceleration
                                        {
                                            X = reading.AccelerationX,
                                            Y = reading.AccelerationY,
                                            Z = reading.AccelerationZ
                                        });
        }

        private bool _sensorIsEnabled;
        public override bool Enabled { 
            get { return _sensorIsEnabled; } 
            set
            {
                if(value == true && _sensorIsEnabled == false)
                    _accelerometer.ReadingChanged += AccelerometerOnReadingChanged;
                else
                    _accelerometer.ReadingChanged -= AccelerometerOnReadingChanged;

                _sensorIsEnabled = value;
            } 
        }

    }
}