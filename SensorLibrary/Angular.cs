namespace SensorLibrary
{
    public class Angular
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }
}