using System;
using Windows.Devices.Sensors;

namespace SensorLibrary
{
    public class InclinometerSensor : Sensor
    {
        public event Action<Tilt> TiltChanged;
 
        private readonly Inclinometer _inclinometer;

        public InclinometerSensor() : base("Inclinometer Sesnor")
        {
            _inclinometer = Inclinometer.GetDefault();

            if (_inclinometer == null) return;

            _inclinometer.ReportInterval = _inclinometer.MinimumReportInterval > 100
                                               ? _inclinometer.MinimumReportInterval
                                               : 100;
        }

        private void InclinometerOnReadingChanged(Inclinometer sender, InclinometerReadingChangedEventArgs args)
        {
            var sensorReading = args.Reading;

            if(TiltChanged!=null)
                TiltChanged(new Tilt
                                {
                                    Roll = sensorReading.RollDegrees,
                                    Pitch = sensorReading.PitchDegrees,
                                    Yaw = sensorReading.YawDegrees
                                });
        }

        private bool _sensorIsEnabled;
        public override bool Enabled { 
            get { return _sensorIsEnabled; } 
            set
            {
                if(value == true && _sensorIsEnabled == false)
                    _inclinometer.ReadingChanged += InclinometerOnReadingChanged;
                else
                    _inclinometer.ReadingChanged -= InclinometerOnReadingChanged;

                _sensorIsEnabled = value;
            } 
        }

    }
}