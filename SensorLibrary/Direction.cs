namespace SensorLibrary
{
    public class Direction
    {
        public double MagneticNorth { get; set; }

        public double? GeographicNorth { get; set; }
    }
}