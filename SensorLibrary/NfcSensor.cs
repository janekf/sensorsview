using System;
using Windows.Networking.Proximity;

namespace SensorLibrary
{
    public class NfcSensor : Sensor
    {
        public event Action<string> PeerFound;
        public event Action PeerLost;

        private readonly ProximityDevice _nfcDevice;

        private const string PEERMESSAGETYPE = "SampleMessage";

        public NfcSensor() : base("NFC Sensor")
        {
            _nfcDevice = ProximityDevice.GetDefault();
        }

        private bool _sensorIsEnabled;

        public override bool Enabled { 
            get { return _sensorIsEnabled; } 
            set
            {
                if(value == true && _sensorIsEnabled == false)
                {
                    _nfcDevice.DeviceArrived += NfcDeviceOnDeviceArrived;
                    _nfcDevice.DeviceDeparted += NfcDeviceOnDeviceDeparted;
                }
                else
                {
                    _nfcDevice.DeviceDeparted -= NfcDeviceOnDeviceDeparted;
                    _nfcDevice.DeviceArrived -= NfcDeviceOnDeviceArrived;
                }

                _sensorIsEnabled = value;
            } 
        }

        private void NfcDeviceOnDeviceArrived(ProximityDevice sender)
        {
            sender.SubscribeForMessage(PEERMESSAGETYPE, (device, message) =>
                                                            {
                                                                var receivedMessage = message.DataAsString;

                                                                if (PeerFound != null)
                                                                    PeerFound(receivedMessage);
                                                            });
        }

        private void NfcDeviceOnDeviceDeparted(ProximityDevice sender)
        {
            if (PeerLost != null)
                PeerLost();
        }
    }
}